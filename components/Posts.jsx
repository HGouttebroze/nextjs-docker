import React from 'react'

export default function Posts() {
  return (
    <div>
        <h3>VIM CMDS MEMO</h3>
        <code>
            <pre>
            `##############################################################################
                # COPYING AND MOVING TEXT
                ##############################################################################


                yw                  yank word
                yy                  yank (copy) a line
                2yy                 yank 2 lines
                y$                  yank to end of line
                p                   put (paste) the clipboard after cursor/current line
                P                   put (paste) before cursor/current line
                :set paste          avoid unexpected effects in pasting
                :registers          display the contents of all registers
                "xyw                yank word into register x
                "xyy                yank line into register x
                :[range]y x         yank [range] lines into register x
                "xp                 put the text from register x after the cursor
                "xP                 put the text from register x before the cursor
                "xgp                just like "p", but leave the cursor just after the new text
                "xgP                just like "P", but leave the cursor just after the new text
                :[line]put x        put the text from register x after [line]


                ##############################################################################
                # MACROS
                ##############################################################################


                qa                  start recording macro 'a'
                q                   end recording macro
                @a                  replay macro 'a'
                @:                  replay last command


                ##############################################################################
                # VISUAL MODE
                ##############################################################################


                v                   start visual mode, mark lines, then do command (such as y-yank)
                V                   start linewise visual mode
                o                   move to other end of marked area
                U                   upper case of marked area
                CTRL-v              start visual block mode
                O                   move to other corner of block
                aw                  mark a word
                ab                  a () block (with braces)
                ab                  a {} block (with brackets)
                ib                  inner () block
                ib                  inner {} block
                Esc                 exit visual mode`
            </pre>
        </code>
    </div>
  )
}
