import React, { useState, useEffect } from 'react'

export default function Fade({ visible, children }) { 
    
    // on ne veut plus rendre les enfants s ils sont caché
    // on créé une var "showChildren" & son setter = par defaut la valeur de la propriété "visible"
    const [showChildren, setShowChildren] = useState(visible);
    
    // avec le hook d'effect, on observe sur la dependance "visible" qd cet props change 
    useEffect(() => { // si visible = aparrait ds le DOM
      if (visible) {
         setShowChildren(true); // passe le setter à true 
      } else { // j créé 1 timer & j l demonte (fcn de retour avec 1 clearTimeOut())
         const timer = setTimeout(() => {
           setShowChildren(false);
         }, 300);
         return () => {
             clearTimeout(timer);
         } 
      }
    }, [visible]);
    let className = 'fade';
    if (!visible) {
        className += ' out';
    }
  return (
    <div className={className}>{showChildren && children}</div>
  )
}
