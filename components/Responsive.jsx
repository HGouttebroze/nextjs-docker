import Image from 'next/image'
import ViewSource from '../components/view-source'
import mountains from '../public/gardon3.jpeg'

const Responsive = () => (
  <div>
    <ViewSource pathname="pages/layout-responsive.js" />
    <h1>Image Component With Layout Responsive</h1>
    <Image
      alt="Picture"
      src={gardon3}
      layout="responsive"
      width={70}
      height={47}
    />
  </div>
)

export default Responsive