import Head from 'next/head'
import { useState } from 'react';
import styles from '../styles/Home.module.css'
import Square from '../components/Square';
import Fade from '../components/Fade';

/*
function Fade({ visible }) {
  const className = `fade ${visible ? "" : "out"}`.trim();
  return (
    <div className={className}>
      <h1 className={styles.title}>
        Welcome to my <a href="https://nextjs.org">Next.js</a> & Docker 
        application !

      </h1>

      <p className={styles.description}>
        {' '}
        <code className={styles.code}>Create animations on React DOM, & Have Fun !</code>
      </p>
    </div>
  )
}
*/
export default function Home({visible, children}) {
  const [open, setOpen] = useState(true);
  const toggle = () => setOpen(o => !o); // inverse la valeur
  
  return (
    <div className={styles.container}>
      <Head>
        <title>Blog Post on Next.js, Docker</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <button onClick={toggle}> SHOW -- HIDE </button>
        <Fade visible={open}>
          <Square /> 
        </Fade>
          <div className={styles.grid}>
            <a href="https://nextjs.org/docs" className={styles.card}>
              <h3>VIM Memo &rarr;</h3>
              <p>
                VIM Modes
              </p>
            </a>
          </div>
          <div className={styles.grid}>
            <a href="https://labs.play-with-docker.com/" className={styles.card}>
              <h3>Docker Swarm : Manager & Worker. , Cluster swarm &rarr;</h3>
              <p>Play with Docker permet de créer des instances, 
                Docker Swarm, manjer, cluster, lister les noeuds: `docker`, utiliser le SSHpermet de joindre un worker sur la machine, Manager, replica, worker, outils de visualisation,, 4 heures d'apprentisage de Docker, Docker Swarm, manjer, cluster, lister les noeuds: `docker`, utiliser le SSH
                Docker Machine, command lgne, permet d'instantier 3 4 machine sur 1 hoste.</p>
            </a>
          </div>
          <div className={styles.grid}>
            <a href="https://nextjs.org/docs" className={styles.card}>
              <h3>Documentation &rarr;</h3>
              <p>Find in-depth information about Next.js features and API.</p>
            </a>
          </div>
        
      </main>

      <footer className={styles.footer}>
        <h3>Here the tools used to design, developped and deploy :  </h3>
  by{' '}
        <ul>
          <li>Next.js, React, React DOM</li>
          <li>StoryBook</li>
          <li>Docker Engine, Docker Composer, Docker Swarm</li>
          <li>Deploy on .....</li>
          <li></li>
        </ul>
          <img src="/gardon4.jpeg" alt="Logo" className={styles.logo} />
      </footer>
    </div>
  )
}
