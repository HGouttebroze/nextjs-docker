This is the Cheat Sheet of JavaScript. It contains explanations, code examples, basic and important operators, functions, principles, methods, and many more. It provides a good overview of the language.

Javascript
JavaScript is a programming language that powers the dynamic behavior on most websites. Alongside HTML and CSS, it is a core technology that makes the web run.

console.log()
The console.log() method is used to log or print messages to the console. It can also be used to print objects and other info.
console.log("Hi there!");
// Prints: Hi there!
Learn about console.log()

Strings
Strings are a primitive data type. They are any grouping of characters (letters, spaces, numbers, or symbols) surrounded by single quotes ' or double quotes " .
let single = 'Wheres my bandit hat?';
let double = "Wheres my bandit hat?";
Learn more about String

Numbers
Numbers are a primitive data type. They include the set of all integers and floating point numbers.
let amount = 6;
let price = 4.99;
Learn more about Numbers

Boolean
Booleans are a primitive data type. They can be either true or false .
let lateToWork = true;
let isHoliday = false;
Learn more about Boolean

Null
Null is a primitive data type. It represents the intentional absence of value. In code, it is represented as null.
let x = null;
Learn more about null

Arithmetic Operators
JavaScript supports arithmetic operators for:

- addition
- subtraction
- multiplication
/ division
% modulo
// Addition
5 + 5;
// Subtraction
10 - 5;
// Multiplication
5 * 10;
// Division
10 / 5;
// Modulo
10 % 5;
Learn more

String.length
The .length property of a string returns the number of characters that make up the string.
let message = "good nite";
console.log(message.length);
// Prints: 9
console.log("howdy".length);
// Prints: 5
Methods
Methods return information about an object, and are called by appending an instance with a period . , the method name, and parentheses.

Libraries
Libraries contain methods that can be called by appending the library name with a period . , the method name, and a set of parentheses.

Math.random()
The Math.random() function returns a floating-point random number in the range from 0 (inclusive) up to but not including 1.
// Returns a number between 0 and 1
Math.random();
More about Math.random()