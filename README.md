# Docker with Next.js 

+ Find informations about tools, Next.js [deployment documentation](https://nextjs.org/docs/deployment#docker-image), use any container-based deployment host.

## How to use

```bash
npx create-next-app 
# or
yarn create next-app 
```

## Using Docker

1. [Install Docker](https://docs.docker.com/get-docker/) on your machine.
1. Build your container: `docker build -t nextjs-docker .`.
1. Run your container: `docker run -p 3000:3000 nextjs-docker`.

You can view your images created with `docker images`.

## Running app



```
yarn dev

yarn build

yarn lint

yarn test
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.

[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on [http://localhost:3000/api/hello](http://localhost:3000/api/hello). This endpoint can be edited in `pages/api/hello.js`.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.

# 1. Docker

# 2. Installation
Installing styled-components only takes a single command and you're ready to roll:

+ with npm
`npm install --save styled-components`

+ with yarn
`yarn add styled-components`

+ If you use a package manager like yarn that supports the "resolutions" package.json field, we also highly recommend you add an entry to it as well corresponding to the major version range. This helps avoid an entire class of problems that arise from multiple versions of styled-components being installed in your project.

In `package.json`:

```js
{
  "resolutions": {
    "styled-components": "^5"
  }
}
{
  "resolutions": {
    "styled-components": "^5"
  }
}
```

+ NOTE
It's highly recommended (but not required) to also use the Babel plugin. It offers many benefits like more legible class names, server-side rendering compatibility, smaller bundles, and more.

## Dockerfile

## Images

### images Distroless

+ permet de réduire la surface d'attaque avec un taille moindre qu'une image alpine

## Principe de couches / layers des images docker 

+ 2 types :
  + en lecture seule
  + lecture / écriture

+ couches peuvent se partager entre les images
  + utilise le cache
  + meilleur      temps de chargement

    illustration:
|image 1     |     image 2| 
couche 1          couche A
couche 2          couche B
          couche 3
couche 4          couche D

les images ont des couches différentes & des couches partagées :

+ écriture :

|conteneur 1     |     conteneur 2| 
R   couche 1              couche A
R   couche 2              couche B
R            couche 3
R   couche 4              couche D
RW  couche 5              couche E

### Exemple : Dockerfile avec empilement par couches

+ on travail sur la conception, par exemple en regroupant ici `git` et `vim` en 1 seul `run`

```yml
FROM ubuntu:latest
RUN apt-get update apt-get install -y --no-install-recommends vim git
```
+ Résultat avec un `docker history` : 
  + on peut voir le gain de couches

+ En mode écriture avec un seul `docker run`

```yml
docker run -tid --name test hugues:v1.0
docker exec -ti test sh
touch heyoh
rm -rf srv/
```
+ que se passe-t-il?

```s
  -$ docker diff test
A /heyoh
D /srv
```
+ NS SOMMES DS LA MEME LOGIQUE QUE GIT
  `docker commit`

### Docker layers et Microservices

+ INTERETS 

# Docker-Compose

+ 4 fichiers :
  + docker compose : orcherstration
  + Dockerfile

```yml
version: '3'
services:
  app:
    build:
    image:
    environnement:
```
+ le Dockerfile : build app
```yml
FROM
WORKDIR /usr/src/app
COPY requirements.txt ./
RUN
COPY
ENV FLASK_APP
CMD flask_run --host=0.0.0.0
``
+ le requirements.txt
```txt
flask
redis
```
+ lancer 1 docker composer:
`docker-composer up -d`
  + -d mode détaché

+ liste des services
`docker-compose ps`

+ liste images
`docker images`

+ stoper service
`docker-compose stop`

+ relancer contener
`docker-compose start`

+ supprimer / nettoyer 
`docker-compose down`

+ do créé 1 bridge et cloisone les container
docker network ls

## Docker & React

```yml
# pull official base image
FROM node:13.12.0-alpine

# set working directory
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install app dependencies
COPY package.json ./
```